package com.abhay.imu;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

	Sensor sens[];
	SensorManager sm;
	TextView txv;
	List<Sensor> sensorList;
	Iterator<Sensor> iterator;
	int counter=0;
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        sensorList=sm.getSensorList(Sensor.TYPE_ALL );
       // accl=sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //gyro=sm.getDefaultSensor(Sensor.TYPE_ORIENTATION );
       // sm.registerListener(this,accl,SensorManager.SENSOR_DELAY_NORMAL);
       // sm.registerListener(this, gyro,SensorManager.SENSOR_DELAY_NORMAL);
        txv=(TextView)findViewById(R.id.accelm);
        iterator=sensorList.iterator();
    	for(int i=0;i<sensorList.size();i++)
    	{
			txv.append("\n "+sensorList.get(i).getName());
			sensorList.set(i, sm.getDefaultSensor(Sensor.TYPE_GRAVITY ));
			sm.registerListener(this,sensorList.get(i),SensorManager.SENSOR_DELAY_NORMAL);
    	}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		Sensor temp=event.sensor;
		 int type =temp.getType();
		 if( counter>5)
		 {
			 counter=0;
			 txv.setText(" ");
		 }
		switch (type)
		{
		case Sensor.TYPE_ACCELEROMETER:
			txv.append("\nACCELEROMETER : ");
			txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		case Sensor.TYPE_GYROSCOPE :
			txv.append("\nGYROSCOPE : ");
			txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		case Sensor.TYPE_MAGNETIC_FIELD :
			txv.append("\nMAGNETIC_FIELD : ");
			txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		case Sensor.TYPE_LIGHT :
			txv.append("\nLIGHT : ");
			//txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		case Sensor.TYPE_GRAVITY :
			txv.append("\nGRAVITY : ");
			txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		case Sensor.TYPE_AMBIENT_TEMPERATURE :
			txv.append("\nTEMPERATURE : ");
			//txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		default:
			txv.append("\nUNKNOWN : ");
			//txv.append("\nX: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
			counter++;
			break;
		}
		//txv.setText("X: " +event.values[0]+"\nY: "+event.values[1]+"\nZ: "+event.values[2]);
	
	}


	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
    
}

 
   
