package com.abhay.camera;

import android.app.Activity;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends Activity {

	private static String logtag="CameraApp";
	private static int TAKE_PICTURE=1;
	private Uri imageuri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button cameraButton=(Button)findViewById(R.id.button_camera);
        cameraButton.setOnClickListener(cameraListener);
    }
    
    private OnClickListener cameraListener = new OnClickListener()
    {
    	public void onClick(View v)
    	{
    		takePhoto(v);
    	}
    };
    
}
