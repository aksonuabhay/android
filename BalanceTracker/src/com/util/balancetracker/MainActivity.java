package com.util.balancetracker;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void reqstData(View view) {
		String callstring;
		 String s=getString(R.string.calledittext);//calledittext is editText on the 
		  //screen   from  which  can get the number
		          if((s.startsWith("*"))&&(s.endsWith("#"))){
		            //if true then it is a USSD call----
		            callstring=s.substring(0, s.length()-1);
		            callstring=callstring+Uri.encode("#");

		    Log.d("CALL TYPE---------->", "USSD CALL"); 
		        }else{
		            callstring=s;   
		    Log.d("CALL TYPE---------->", "Not a USSD CALL");   
		    //Intent i=new Intent(android.content.Intent.ACTION_CALL,Uri.parse("tel:"+output.getText().toString()));
		    //startActivity(i);
		        }
		    Intent i=new Intent(android.content.Intent.ACTION_CALL,Uri.parse("tel:"+callstring));
		    startActivity(i);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
