package com.example.speakingApp;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

/**
 * A very simple application to handle Voice Recognition intents
 * and display the results
 */
public class MyActivity extends Activity
{

    private static final int REQUEST_CODE = 1234;
    private ListView cmdList;
    private String cmd;
    private TextView text;
    final ArrayList<String> commands= new ArrayList<String>();
    /**
     * Called with the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        text= (TextView) findViewById(R.id.editText);
        Button speakButton = (Button) findViewById(R.id.speakButton);

        cmdList = (ListView) findViewById(R.id.list);
        commands.add("Light ON");
        commands.add("Light OFF");
        commands.add("Fan ON");
        commands.add("Fan OFF");
        commands.add("ON Everything");
        cmdList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                commands));

        cmdList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String s= Integer.toString(position+1);
                text.setText(commands.get(position));
                call(s);


            }
        });

        // Disable button if no recognition service is present
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() == 0)
        {
            speakButton.setEnabled(false);
            speakButton.setText("Recognizer not present");
        }
    }

    /**
     * Handle the action of the button being clicked
     */
    public void speakButtonClicked(View v)
    {
        startVoiceRecognitionActivity();
    }

    /**
     * Fire an intent to start the voice recognition activity.
     */
    private void startVoiceRecognitionActivity()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Voice recognition Demo...");
        startActivityForResult(intent, REQUEST_CODE);
    }

    /**
     * Handle the results from the voice recognition activity.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    { ArrayList<String> matches=null;
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK)
        {
            // Populate the wordsList with the String values the recognition engine thought it heard
            matches = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);

        }
        super.onActivityResult(requestCode, resultCode, data);
        for(int i=0; i<5; i++){
            if(matches.get(i).contains("light")&& matches.get(i).contains("on")) {cmd="1"; break;}
            else if(matches.get(i).contains("light")&& matches.get(i).contains("off")) {cmd="2"; break;}
            else if(matches.get(i).contains("light")&& matches.get(i).contains("on")) {cmd="3"; break;}
            else if(matches.get(i).contains("light")&& matches.get(i).contains("on")) {cmd="4"; break;}
            else if(matches.get(i).contains("light")&& matches.get(i).contains("on")) {cmd="5"; break;}

        }
        call(cmd);
    }

    public void call(String s ){
        text.setText(commands.get(Integer.parseInt(s)-1));
        SendCommand sc= new SendCommand();
        sc.Start(s);

    }



}
